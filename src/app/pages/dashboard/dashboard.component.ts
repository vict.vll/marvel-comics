import { Component, OnInit } from '@angular/core';
import { ComicService } from '../../services/comic/comic.service';

import { FavoritesService } from 'src/app/services/favorites/favorites.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  items: any;
  selectedComic: any;
  infoType: number;
  comics: Array<Object>;
  constructor(private _comic: ComicService, private _favorites: FavoritesService) {
    this.comics = [];
    this.infoType = 0;
   }

  ngOnInit(): void {
    this.getComics();
  }

  showItems(items: any) {
    this.items = items.data;
  }

  openComic(resourceURI: any) {
    this._comic.getComicFromURL(resourceURI).subscribe(response => {
      this.selectedComic = response?.data.results[0];
      this.infoType = 1;
    });
  }
  openDetail(item: any) {
    this.selectedComic = item;
    this.infoType = 2;
  }

  closeModal() {
    this.selectedComic = null;
  }

  getComics() {
    this.comics = this._favorites.getFavorites();
  }
}

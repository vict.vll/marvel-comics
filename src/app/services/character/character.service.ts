import { Injectable } from '@angular/core';
import { HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Constants } from "../../config/constants";
import {Md5} from 'ts-md5/dist/md5';
const md5 = new Md5();

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  API = Constants.MARVEL_API;
  apikey = Constants.MARVEL_API.apikey;
  constructor(
    private http: HttpClient) { }

  getCharacter(searchText: string, limit: number = 10) {
    const ts = Date.now();
    const hash = md5.start().appendStr(ts+this.API.privatekey+this.API.apikey).end();
    return this.http.get(`${this.API.URL}${this.API.VERSION}${this.API.CHARACTERS}?nameStartsWith=${searchText}&limit=${limit}&ts=${ts}&apikey=${this.apikey}&hash=${hash}`);
  }
  
}

import { Injectable } from '@angular/core';
import { HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Constants } from "../../config/constants";
import {Md5} from 'ts-md5/dist/md5';
import { Observable } from 'rxjs';
const md5 = new Md5();

@Injectable({
  providedIn: 'root'
})
export class ComicService {

  API = Constants.MARVEL_API;
  apikey = Constants.MARVEL_API.apikey;
  constructor(
    private http: HttpClient) { }

  getComicFromURL(url: string):Observable<any> {
    const ts = Date.now();
    const hash = md5.start().appendStr(ts+this.API.privatekey+this.API.apikey).end();
    return this.http.get(`${url}?ts=${ts}&apikey=${this.apikey}&hash=${hash}`);
  }
  
}

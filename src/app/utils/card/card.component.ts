import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() item: any;
  @Output() comic = new EventEmitter<any>();
  @Output() detail = new EventEmitter<any>();

  constructor() { 
  }

  ngOnInit(): void {
  }

  sendComic(resourceURI: string) {
    this.comic.emit(resourceURI);
  }
  sendCharacter(item: any) {
    this.detail.emit(item);
  }

}
